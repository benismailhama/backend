<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class workshop_evaluation extends Model
{
    protected $table = 'Workshop_Evaluation';
    protected $primaryKey = 'workshop_evaluation_id';
    protected $fillable = ['workshop_id','item_evaluation_id'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
