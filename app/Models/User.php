<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    protected $table = 'User';
    protected $primaryKey = 'user_id';
    protected $fillable = ['first_name', 'last_name', 'email' ,'points' ,'privilege_id','url_photo','membership_id'];
    public $timestamps = true;

    protected $hidden = ['password','verification_code'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at','reseted_at'];



    public function user_workshops()
    {
        return $this->hasMany(WorkshopUser::class, 'user_id', 'user_id');

    }
    public function workshops()
    {
        return $this->belongsToMany(Workshop::class,'workshop_user','user_id','workshop_id');

    }

}
