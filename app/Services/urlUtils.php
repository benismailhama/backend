<?php


namespace App\Services;


use Illuminate\Support\Facades\App;

class UrlUtils
{

    public static function getFrontUrl()
    {
        if (App::environment() == 'test') {
            return "http://localhost:4200";
        }
        if (App::environment() == 'prod') {
            return "https://securinets.com";
        }
        if (App::environment() == 'dev') {
            return "https://meet-securinets.me";
        }
        return "http://localhost:4200";
    }
    public static function getLoginFront() {
        if (App::environment() == 'test') {
            return "http://localhost:4200/auth/login";
        }
        if (App::environment() == 'prod') {
            return "https://securinets.com/auth/login";
        }
        if (App::environment() == 'dev') {
            return "https://meet-securinets.me/auth/login";
        }
        return "http://localhost:4200/auth/login";
    }
    
    public static function getBackendUrl()
    {
        if (App::environment() == 'test') {
            return "http://localhost:8000/api";
        }
        if (App::environment() == 'prod') {
            return "https://securinets.com/api";
        }
        if (App::environment() == 'dev') {
            return "https://meet-securinets.me/api";
        }
        return "http://localhost:8000/api";
    }
}