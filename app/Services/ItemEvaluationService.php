<?php

namespace App\Services;

use App\Models\ItemEvaluation;
use App\Models\workshop_evaluation;
use Illuminate\Support\Facades\Storage;

class ItemEvaluationService
{

    public function addItem($label,$user_id) {

        $itemEv = new ItemEvaluation();
        $itemEv->label = $label;
        $itemEv->user_id = $user_id;
        $itemEv->save();
        return $itemEv;
    }

    public function editItem($item , $label) {
        $item->label = $label;
        $item->update();
        return $item;
    }

    public function deleteItem($item_id) {
        ItemEvaluation::where('item_evaluation_id','=',$item_id)->first()->delete();
    }
    public function addWorkShopEvaluation($item_id,$workshop_id) {
        $workShopEva = new workshop_evaluation();
        $workShopEva->item_evaluation_id = $item_id;
        $workShopEva->workshop_id = $workshop_id;
        $workShopEva->save();
        return $workShopEva;
    }
}