<?php

namespace App\Imports;

use App\Models\Membership;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Services\UserServices;
class MemberhipsImpot implements ToModel
{
    protected $userServices;
    function __construct(UserServices $userServices)
    {
        $this->userServices = $userServices;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if($row[0] && $row[1]) {
         if (!$this->userServices->getMembershipByIndexAndByEmail($row[0],$row[1])) {
        return new Membership([
            'membership_index' => $row[0],
            'email' => $row[1] 
        ]);
         }
        }
    }
}
