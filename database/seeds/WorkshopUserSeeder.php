<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkshopUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Workshop_User')->insert([
            'user_id' => 3,
            'workshop_id' => 1,
            'is_present' => 0,

        ]);
        DB::table('Workshop_User')->insert([
            'user_id' => 3,
            'workshop_id' => 2,
            'is_present' => 0,

        ]);
    }
}
