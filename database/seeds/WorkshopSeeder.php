<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkshopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Workshop')->insert([
            'name' => 'Workshop 1',
            'description' => 'test',
            'moderator_id' => '1',
            'class_room' => 'Salle 243',
            'duration' => 120,
            'max_places' => 30,
            'available_places' => 30,
            'start_date' => date("Y-m-d")
            ]);
        DB::table('Workshop')->insert([
            'name' => 'Workshop 2',
            'description' => 'test',
            'moderator_id' => '2',
            'class_room' => 'Salle 145',
            'duration' => 90,
            'max_places' => 20,
            'available_places' => 20,
            'start_date' => date("Y-m-d")
        ]);
        DB::table('Workshop')->insert([
            'name' => 'Workshop 3',
            'description' => 'test',
            'moderator_id' => '4',
            'class_room' => 'Hall',
            'duration' => 120,
            'max_places' => 30,
            'available_places' => 30,
            'start_date' => date("Y-m-d")
        ]);

    }
}
