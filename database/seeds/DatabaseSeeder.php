<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PrivilegeTableSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(WorkshopSeeder::class);
         $this->call(WorkshopUserSeeder::class);
    }
}
