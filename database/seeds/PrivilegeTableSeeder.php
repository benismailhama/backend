<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PrivilegeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Privilege')->insert([
            'privilege_id' => 1,
            'label' => 'Admin'
        ]);
        DB::table('Privilege')->insert([
            'privilege_id' => 2,
            'label' => 'Moderator'
        
        ]) ;
        DB::table('Privilege')->insert([
            'privilege_id' => 3,
            'label' => 'Member'
        
        ]);
    }
}
