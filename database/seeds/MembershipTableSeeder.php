<?php

use App\Models\Membership;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MembershipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for ($i = 1 ; $i <= 114 ; $i++)
        DB::table('Membership')->updateOrInsert(
            ['membership_id' => $i, 'membership_index' => $i],
            ['membership_id' => $i, 'membership_index' => $i]
        );
    }
    
}
