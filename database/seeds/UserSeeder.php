<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('User')->insert([
            'first_name' => 'Moez',
            'last_name' => 'Saidi',
            'email' => 'Moez@gmail.com',
            'points' => 0,
            'privilege_id' => 1,
            'password' => '$2y$10$6wz7ke5g9JSLubP/aw0WKu4KTV/uhc8gyppKWxnxclVy944HXr4sq',
        ]);

        DB::table('User')->insert([
            'first_name' => 'Hama',
            'last_name' => 'Ben Ismaïl',
            'email' => 'Hama@gmail.com',
            'points' => 0,
            'privilege_id' => 1,
            'password' => '$2y$10$6wz7ke5g9JSLubP/aw0WKu4KTV/uhc8gyppKWxnxclVy944HXr4sq',
        ]);

        DB::table('User')->insert([
            'first_name' => 'Omar',
            'last_name' => 'Chaabouni',
            'email' => '123456789omar@gmail.com',
            'points' => 0,
            'privilege_id' => 3,
            'password' => '$2y$10$6wz7ke5g9JSLubP/aw0WKu4KTV/uhc8gyppKWxnxclVy944HXr4sq',
        ]);

        DB::table('User')->insert([
            'first_name' => 'Ghaida',
            'last_name' => 'Bouchaala',
            'email' => 'ghaida@gmail.com',
            'points' => 0,
            'privilege_id' => 2,
            'password' => '$2y$10$6wz7ke5g9JSLubP/aw0WKu4KTV/uhc8gyppKWxnxclVy944HXr4sq',
        ]);
    }
}
