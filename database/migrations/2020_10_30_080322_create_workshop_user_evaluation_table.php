<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkshopUserEvaluationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Workshop_User_Evaluation', function (Blueprint $table) {
            $table->increments('workshop_user_evaluation_id');
            $table->integer('item_evaluation_id')->unsigned();
            $table->foreign('item_evaluation_id')->references('item_evaluation_id')->on('Item_Evaluation')->onDelete('cascade');
            $table->integer('workshop_user_id')->unsigned();
            $table->foreign('workshop_user_id')->references('workshop_user_id')->on('Workshop_User')->onDelete('cascade');
            $table->integer('note')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Workshop_User_Evaluation');
    }
}
